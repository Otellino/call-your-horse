Scriptname CYH_LastRiddenHorseWatcherScript extends Quest  

ReferenceAlias Property PlayerOwnedLastRiddenHorse Auto
Faction Property PlayerHorseFaction Auto
Faction Property PlayerFaction Auto
ActorBase Property Shadowmere Auto

actor playerRef
actor currentHorse

Function setup()
	; Store player.
	playerRef = game.GetPlayer()

	; Register for the player riding a horse.
	RegisterForAnimationEvent(playerRef, "tailHorseMount")

	; Store the player's current owned horse.
	if playerOwnsCurrentHorse()
		PlayerOwnedLastRiddenHorse.forceRefTo(currentHorse)
	endif
EndFunction

Bool Function playerOwnsCurrentHorse()
	; This function checks that the horse being ridden by the player is owned by them - either directly, or is in their faction / player horse faction.
	currentHorse = game.GetPlayersLastRiddenHorse()
		
	if currentHorse.getActorOwner() == playerRef.getActorBase() || currentHorse.getFactionOwner() == PlayerFaction || currentHorse.isInFaction(PlayerHorseFaction) || currentHorse.isInFaction(PlayerFaction) || currentHorse.GetActorBase() == Shadowmere
		return true
	else
		return false
	endif
EndFunction

Event onAnimationEvent(ObjectReference akSource, string asEventName)
	if akSource == playerRef
		if asEventName == "tailHorseMount"
			if playerOwnsCurrentHorse()
				PlayerOwnedLastRiddenHorse.forceRefTo(currentHorse)
			endif
		endif
	endif
EndEvent