Scriptname CYH_UpdateManagerAliasScript extends ReferenceAlias  
{Script attached to the Player alias on CYH_UpdateManager to send OnPlayerLoadGame events to run any necessary update functionality.}

CYH_UpdateManagerScript Property CYH_UpdateManager Auto

Event onPlayerLoadGame()
	CYH_UpdateManager.updateCheck()
EndEvent