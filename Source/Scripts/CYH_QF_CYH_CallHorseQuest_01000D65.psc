;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 9
Scriptname CYH_QF_CYH_CallHorseQuest_01000D65 Extends Quest Hidden

;BEGIN ALIAS PROPERTY StableHorse
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_StableHorse Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY PlayerOwnedLastRiddenHorse
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_PlayerOwnedLastRiddenHorse Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Horse
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Horse Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY PlayerOverrideHorse
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_PlayerOverrideHorse Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_2
Function Fragment_2()
;BEGIN AUTOCAST TYPE CYH_CallHorseQuestScript
Quest __temp = self as Quest
CYH_CallHorseQuestScript kmyQuest = __temp as CYH_CallHorseQuestScript
;END AUTOCAST
;BEGIN CODE
; Horse arrival behaviour finished, shut down quest.
stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_4
Function Fragment_4()
;BEGIN AUTOCAST TYPE CYH_CallHorseQuestScript
Quest __temp = self as Quest
CYH_CallHorseQuestScript kmyQuest = __temp as CYH_CallHorseQuestScript
;END AUTOCAST
;BEGIN CODE
; Horse has arrived. Call function which handles the Loiter Quest starting.
kmyQuest.horseArrived()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN AUTOCAST TYPE CYH_CallHorseQuestScript
Quest __temp = self as Quest
CYH_CallHorseQuestScript kmyQuest = __temp as CYH_CallHorseQuestScript
;END AUTOCAST
;BEGIN CODE
; "Call Horse" Ability used.
; Stop the "Horse Loitering" quest.
CYH_CallHorseLoiterQuest.stop()
; Run the "Horse Called" logic.
kmyQuest.HorseCalled()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Quest Property CYH_CallHorseLoiterQuest  Auto  
