Scriptname CYH_CallHorseEffectScript extends activemagiceffect  

Keyword Property CYH_CallHorseKeyword Auto
Sound Property CYH_CallHorseWhistle Auto

Event OnEffectStart(Actor akTarget, Actor akCaster)
	int instanceID = CYH_CallHorseWhistle.Play((akTarget as ObjectReference))
	CYH_CallHorseKeyword.SendStoryEvent()
endEvent