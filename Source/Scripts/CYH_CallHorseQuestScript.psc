Scriptname CYH_CallHorseQuestScript extends Quest  

; ============= Script Properties =============
ReferenceAlias Property Horse Auto
ReferenceAlias Property StableHorse Auto
ReferenceAlias Property PlayerOwnedLastRiddenHorse Auto
ReferenceAlias Property PlayerOverrideHorse Auto

WorldSpace Property Tamriel Auto
Message Property CYH_CallHorseInvalidMsg Auto
Message Property CYH_CallHorseNoHorseMsg Auto
Quest Property CYH_CallHorseLoiterQuest Auto
Bool Property HorseTravelFinished = false Auto
; =============================================

; ========= Horse teleport parameters =========
float moveDistance = 2048.0 ; How far behind the player should the Horse be teleported to?
float teleportMinDistance = 4096.0 ; How far from the horse should the player be before we allow the horse to teleport?
; =============================================

; ============ Horse stuck checks =============
float stuckCheckTime = 10.0 ; How many seconds should pass after a teleport before the horse is checked to see if it is stuck?
float horseStuckThreshold = 96.0 ; How close to the horse's teleport location must the horse still be before it's considered stuck?
float horsePosX = 0.0
float horsePosY = 0.0
; =============================================

actor playerRef
objectReference horseRef

Function horseCalled()
	; Fill the Horse alias with the appropriate horse.
	Horse.forceRefTo(currentPlayerHorse())

	; Store references.
	playerRef = Game.GetPlayer()
	horseRef = Horse.GetReference()
	
	; Make sure the player has a living horse.
	if !(horseRef == none)
		; Make sure the player is in a valid location before calling the horse.
		if IsValidLocation()
			; If the player is close to the horse, we don't want to teleport the horse - only let it move on its own.
			if !(playerRef.HasLOS(horseRef) || playerRef.getDistance(horseRef) <= teleportMinDistance)    			
    			moveHorseToPlayer(180, moveDistance)
    		endif
		else
			CYH_CallHorseInvalidMsg.show()
			setStage(1000)
		endif
	else
		CYH_CallHorseNoHorseMsg.show()
		setStage(1000)
	endif
EndFunction

; Return the most appropriate player-owned horse.
actor Function currentPlayerHorse()
	actor playerOverrideHorseRef = PlayerOverrideHorse.getActorRef()
	actor playerOwnedLastRiddenHorseRef = PlayerOwnedLastRiddenHorse.getActorRef()
	actor playerStableHorseRef = StableHorse.getActorRef()

	if playerOverrideHorseRef && !playerOverrideHorseRef.isDead()
		; Currently this will never run, might be added as a future feature.
		return playerOverrideHorseRef
	else
		; If the player has an owned last ridden horse, use that. If not, fall back to one of their stable horses.
		if playerOwnedLastRiddenHorseRef && !playerOwnedLastRiddenHorseRef.isDead() && !playerOwnedLastRiddenHorseRef.isDisabled()
			return playerOwnedLastRiddenHorseRef
		elseif !playerStableHorseRef.isDead() && !playerStableHorseRef.isDisabled()
			return playerStableHorseRef
		endif
	endif
EndFunction

Function moveHorseToPlayer(float angleToMove, float distanceToMove)
    ;0 forward, 180 back, 90 left, 270 right (can't use negative numbers)

    float offsetX = distanceToMove * Math.Sin(playerRef.getAngleZ() + angleToMove)
    float offsetY = distanceToMove * Math.Cos(playerRef.getAngleZ() + angleToMove)
    
    horseRef.moveTo(playerRef, offsetX, offsetY, 0, true)

    horsePosX = horseRef.X
	horsePosY = horseRef.Y

	; After a few seconds, check to see if the Horse has moved from the spawn location. If not, it's probably stuck.
    registerForSingleUpdate(stuckCheckTime)
EndFunction

; The horse has arrived. Try to start the Horse Loitering behaviour quest. If this fails, we skip straight to the shut down stage for the Call Horse quest.
Function horseArrived()
	if !CYH_CallHorseLoiterQuest.start()
		setStage(1000)
	endif
EndFunction

; Check that the player is in Skyrim and not in an interior before allowing the Horse to be called.
Bool Function isValidLocation()
	if !playerRef.isInInterior()
		if playerRef.GetWorldspace() == Tamriel
			return true
		else 
			return false
		endif
	else
		return false
	endif
EndFunction

; Check if the Horse is stuck by comparing its previous coordinates to its current ones, with a slight buffer.
Bool Function isHorseStuck()
	float previoushorsePosX = horsePosX
	float previoushorsePosY = horsePosY

	horsePosX = horseRef.X
	horsePosY = horseRef.Y

	float posXMinimum = horsePosX - horseStuckThreshold 
	float posXMaximum = horsePosX + horseStuckThreshold 

	float posYMinimum = horsePosY - horseStuckThreshold 
	float posYMaximum = horsePosY + horseStuckThreshold 

	if ((previoushorsePosX < PosXMaximum) && (previoushorsePosX > PosXMinimum)) && ((previoushorsePosY < PosYMaximum) && (previoushorsePosY > PosYMinimum))
		; Horse is stuck!
		return true
	else
		; Horse is moving, so it's probably not stuck.
		return false
	endif
EndFunction

Event onUpdate()
	; If the Horse has arrived, we don't need to run this check anymore.
	if HorseTravelFinished == false
		if isHorseStuck()
			moveHorseToPlayer(90, 64)
			unregisterForUpdate()
		endif
	else
		unregisterForUpdate()
	endif
EndEvent