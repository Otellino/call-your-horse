;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 3
Scriptname QF_CYH_LastRiddenHorseWatche_01000801 Extends Quest Hidden

;BEGIN ALIAS PROPERTY PlayerOwnedLastRiddenHorse
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_PlayerOwnedLastRiddenHorse Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN AUTOCAST TYPE CYH_LastRiddenHorseWatcherScript
Quest __temp = self as Quest
CYH_LastRiddenHorseWatcherScript kmyQuest = __temp as CYH_LastRiddenHorseWatcherScript
;END AUTOCAST
;BEGIN CODE
kmyQuest.Setup()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
