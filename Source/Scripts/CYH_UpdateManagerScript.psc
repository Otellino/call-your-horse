Scriptname CYH_UpdateManagerScript extends Quest  
{Script attached to CYH_UpdateManager to run any necessary update bugfixes.}

GlobalVariable Property CYH_Version Auto

; Update fixes properties.
Quest Property CYH_AddAbilityQuest Auto
Quest Property CYH_CallHorseLoiterQuest Auto
Quest Property CYH_CallHorseQuest Auto
Quest Property CYH_LastRiddenHorseWatcher Auto
Spell Property CYH_CallHorsePower Auto

Float storedVersion

Event onInit()
	start()
	updateCheck()
EndEvent

Function updateCheck()
	float currentVersion = CYH_Version.getValue()
	; If the mod's version is higher than the currently stored version, run any necessary update functionality.
	if storedVersion < currentVersion
		if storedVersion < 1.01
			; Version 1.0.1 adds a new property to CYH_LastRiddenHorseWatcher which requires the quest to re-start in existing saves. Do so here.
			CYH_LastRiddenHorseWatcher.stop()
			CYH_LastRiddenHorseWatcher.start()
		endif

		; Update the stored version number.
		storedVersion = currentVersion
	endif
EndFunction